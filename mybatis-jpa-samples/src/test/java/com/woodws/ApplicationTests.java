package com.woodws;

import com.woodws.mybatis.samples.Application;
import com.woodws.mybatis.samples.entity.User;
import com.woodws.mybatis.samples.dao.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
public class ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    @Commit
    public void testUserMapper() throws Exception {
        Date date = userMapper.funcSysdate();
        User user = new User("test",8,"13900000001",0);
        User user1 = new User("test1",9,"13900000002",1);
        User user2 = new User("test2",10,"13900000003",2);
        User user3 = new User("test3",11,"13900000004",3);
        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        Object o1 = userMapper.insert(user);
        Object o2 = userMapper.insertBatch(users);
        Object o3 = userMapper.find(user.getId());
        Object o4 = userMapper.findAll();
        Object o5 = userMapper.findByNameOrderByAge("test");
        Object o6 = userMapper.findOrderByAge();
        Object o7 = userMapper.findNameById(user.getId());
        Object o8 = userMapper.findOrderByAgeDesc();
        Object o9 = userMapper.findByNameOrAgeGreaterOrderByAge("test1",10);
        Object o10 = userMapper.update(user);
        Object o11 = userMapper.updateName(user);
        Object o12 = userMapper.delete(user.getId());

        Object o14 = userMapper.findByNameLike("test");

        //Object o13 = userMapper.deleteAll();
        System.out.print(1);

    }


}
